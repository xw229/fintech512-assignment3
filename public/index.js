// var ready = (callback) => {
//     if (document.readyState != "loading") callback();
//     else document.addEventListener("DOMContentLoaded", callback);
//   }
  
//   ready(() => { 
//       /* Do things after DOM has fully loaded */ 
//       makeplot();
//   });

// function makeplot() {
//     console.log("makeplot: start")
//     // var x = document.getElementById("stock").value
//     // fetch(`${x}.csv`)
//     fetch("AAPL.csv")
//     .then((response) => response.text()) /* asynchronous */
//     .catch(error => {
//         alert(error)
//          })
//     .then((text) => {
//       console.log("csv: start")
//       csv().fromString(text).then(processData)  /* asynchronous */
//       console.log("csv: end")
//     })
//     console.log("makeplot: end")
  
// };

// function processData(data) {
//   console.log("processData: start")
//   let x = [], y = []

//   for (let i=0; i<data.length; i++) {
//       row = data[i];
//       x.push( row['Date'] );
//       y.push( row['Close'] );
//   } 
//   makePlotly( x, y );
//   console.log("processData: end")
// }


var s = ""

var ready = (callback) => {
    if (document.readyState != "loading") callback();
    else document.addEventListener("DOMContentLoaded", callback);
}
  
ready(() => { 
    /* Do things after DOM has fully loaded */ 
    makeplot();
});

function makeplot() {
    console.log("makeplot: start")
    // fetch("AAPL.csv")
    s = document.getElementById("stock").value
    fetch(`${s}.csv`)
    .then((response) => response.text()) /* asynchronous */
    .catch(error => {
        alert(error)
        })
    .then((text) => {
      console.log("csv: start")
      csv().fromString(text).then(processData)  /* asynchronous */
      console.log("csv: end")
    })
    console.log("makeplot: end")
  
};

function processData(data) {
  console.log("processData: start")
  let x = [], y = []

  for (let i=0; i<data.length; i++) {
      row = data[i];
      x.push( row['Date'] );
      y.push( row['Close'] );
  } 
  makePlotly( x, y );
  console.log("processData: end")
}

function makePlotly( x, y){
  console.log("makePlotly: start")
  var traces = [{
        x: x,
        y: y
  }];
  var layout  = { title: `${s}`}

  myDiv = document.getElementById('myDiv');
  Plotly.newPlot( myDiv, traces, layout );
  console.log("makePlotly: end")
};